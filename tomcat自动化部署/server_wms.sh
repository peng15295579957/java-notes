#!/bin/bash
set -m  

tomcatPath=/wms/apache-tomcat-8080
appPath=$tomcatPath"/webapps/WMS"
backupPath=$tomcatPath"/backup/WMS"$(date "+%Y%m%d_%H%M%S")
warPath=/wms/WMS.war
configPath=$tomcatPath"/config_test.zip"

    echo "[ ==这是帮助信息== ]"
    echo " ---> 1.使用deploy部署项目"
    echo " ---> 2.使用kill可以停止tomcat"
    echo " ---> 3.使用start启动tomcat"
    echo " ---> 4.使用默认配置重启tomcat"
    echo " ---> 5.使用log显示日志"
    echo " ---> 6.使用port显示端口"
echo " --->"
echo " ---> tomcat目录的位置为$tomcatPath"
#echo " ---> 设置命令 dspt_dir为跳转到tomcat目录"
echo " --->"
#echo "alias dspt_dir='cd $tomcatPath'" > .bashrc
port=$(ps -ef | grep $tomcatPath | awk '{print $2}')
echo " ---> Tomcat run at port:$port"
if [ $1 == "kill" ];then
    echo " ---> 正在kill tomcat进程！"
    kill -9 $port
    exit 0
fi
if [ $1 == "start" ];then
    echo " ---> 正常启动tomcat服务"
    $tomcatPath"/bin/startup.sh"
    exit 0
fi
if [ $1 == "log" ];then
    tail -f $tomcatPath"/logs/catalina.out"
    exit 0;
fi
if [ $1 == "port" ];then
    echo " ---> 该tomcat所有的端口列表"
    ps -ef | grep $tomcatPath | awk '{print $2}'
    exit 0;
fi
if [ $1 == "restart" ];then
    echo " ---> 正在重启Tomcat"
    kill -9 $port
    $tomcatPath/bin/startup.sh &
    tail -f $tomcatPath"/logs/catalina.out"
    exit 0
fi
if [ $1 == "deploy" ];then
    echo " ---> 正在重新部署Tomcat"
    kill -9 $port
    mv $appPath $backupPath
    echo " ---> 备份路径"$backupPath
    unzip $warPath -d $appPath
    unzip -o $configPath -d $appPath"/WEB-INF/classes/"
    $tomcatPath/bin/startup.sh 
    tail -f $tomcatPath"/logs/catalina.out"
    exit 0
fi


